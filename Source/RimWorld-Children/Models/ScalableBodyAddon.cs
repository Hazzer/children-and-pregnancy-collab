﻿namespace RimWorldChildren {
    using RimWorld;
    using UnityEngine;
    using Verse;

    /// <summary>
    /// Model containing scaling and offset information for Alien Races Body Addons.
    /// All offsets and scales are in addition to what is already defined for Alien Races
    /// </summary>
    public class ScalableBodyAddon {
        // Required: The body part name to map to alien races def for part scaling
        public string bodyPart;

        // Optional: Use the specified body type as a reference for creating offsets
        public BodyTypeDef referenceBody;

        // Optional: Whether or not to render the addon at this lifestage
        public bool renderPart = true;

        /// <summary>
        /// These fields are the scale and offset adjustments to be applied to this body part
        /// based on the direction the pawn is facing. Directional adjustments are
        /// required as HAR allows developers to specify offsets per direction as well.
        /// If needed, we will also specify body type here.
        /// </summary>
        public RotationalAdjuster north = new RotationalAdjuster();
        public RotationalAdjuster south = new RotationalAdjuster();
        public RotationalAdjuster east = new RotationalAdjuster();
        public RotationalAdjuster west = new RotationalAdjuster();

        /// <summary>
        /// Get an adjuster based on current pawn facing
        /// </summary>
        /// <param name="rotation">The pawn facing direction</param>
        /// <returns>An appropriate adjuster based on facing</returns>
        public RotationalAdjuster GetDirectionalAdjuster(Rot4 rotation) {
            return rotation == Rot4.South ? south :
                                            rotation == Rot4.North ? north :
                                            rotation == Rot4.East ? east : west;
        }
    }
}
